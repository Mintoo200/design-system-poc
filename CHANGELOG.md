### [1.7.3](https://gitlab.com/Mintoo200/design-system/compare/v1.7.2...v1.7.3) (2021-11-21)


### Bug Fixes

* variables! ([dfc7e21](https://gitlab.com/Mintoo200/design-system/commit/dfc7e21ea892f996d0abd065cf370ea277c017e0))

### [1.7.2](https://gitlab.com/Mintoo200/design-system/compare/v1.7.1...v1.7.2) (2021-11-21)


### Bug Fixes

* variables? ([9d2d06b](https://gitlab.com/Mintoo200/design-system/commit/9d2d06b798d0dd5bfbd395ec09bd38002d2dbd51))

### [1.7.1](https://gitlab.com/Mintoo200/design-system/compare/v1.7.0...v1.7.1) (2021-11-21)


### Bug Fixes

* sed ? ([42898f9](https://gitlab.com/Mintoo200/design-system/commit/42898f90574d072ce1e2e3e507b7547cfddbbe3b))

## [1.7.0](https://gitlab.com/Mintoo200/design-system/compare/v1.6.0...v1.7.0) (2021-11-21)


### Features

* Test committer ([5ee40d8](https://gitlab.com/Mintoo200/design-system/commit/5ee40d82e47b01ebff2034de3c98a8bff9834d6b))
* test sed ([144e202](https://gitlab.com/Mintoo200/design-system/commit/144e202da0d1120b4441e8ec825071b5fa31aad4))

## [1.6.0](https://gitlab.com/Mintoo200/design-system/compare/v1.5.0...v1.6.0) (2021-11-20)


### Features

* truc ([dfe116f](https://gitlab.com/Mintoo200/design-system/commit/dfe116f3161eed4546303532c5b940aaa309b393))
