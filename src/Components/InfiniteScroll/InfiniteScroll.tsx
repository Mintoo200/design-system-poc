import React, { useEffect, useRef, useState } from 'react'

export type Props = {
  children: React.ReactNode,
  getMoreResults: () => void,
  threshold?: number,
}

function isCloseToEnd(element: HTMLElement, threshold: number) {
  const boundingRect = element.getBoundingClientRect()
  const viewportHeight = window.innerHeight || document.documentElement.clientHeight
  console.log(`${boundingRect.bottom} - ${threshold} <= ${(viewportHeight)}`)
  return (
    (boundingRect.bottom - threshold) <= (viewportHeight)
  )
} 

export function InfiniteScroll({ children, getMoreResults, threshold = 0 }: Props) {
  const ref = useRef<HTMLElement>(null)
  const [closeToEnd, setCloseToEnd] = useState(false)

  useEffect(() => {
    function checkScrollLocation() {
      if (!closeToEnd && ref.current != null && isCloseToEnd(ref.current, threshold)) {
        setCloseToEnd(true)
      } else if (closeToEnd && ref.current != null && !isCloseToEnd(ref.current, threshold)) {
        setCloseToEnd(false)
      }
    }
    checkScrollLocation()
    window.addEventListener('scroll', checkScrollLocation)
    return () => window.removeEventListener('scroll', checkScrollLocation)
  }, [ref, closeToEnd, setCloseToEnd, threshold])

  useEffect(() => {
    if (closeToEnd) {
      getMoreResults()
    }
  }, [closeToEnd])

  return (
    <section ref={ref}>
      {children}
    </section>
  )
}